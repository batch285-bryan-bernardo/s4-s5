package com.zuitt.activity;

import java.util.ArrayList;

public class Phonebook {


    private ArrayList<Contact> contacts;


    public Phonebook(){
        contacts = new ArrayList<Contact>();
    };

    public Phonebook(ArrayList<Contact> contacts){
      this.contacts = contacts;
    };

    // Setters and Getters

    public void setPhonebook(Contact contact){
        contacts.add(contact);
    }

    public ArrayList<Contact> getPhonebook(){
        if (contacts.isEmpty()){
            System.out.println("Your Phonebook is empty!");
        }
        return contacts;
    }


}
