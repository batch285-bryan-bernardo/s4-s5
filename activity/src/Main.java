import com.zuitt.activity.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Phonebook myPhonebook = new Phonebook();

        while (true) {

            System.out.print("Enter name: ");
            String name = scanner.nextLine();

            System.out.print("Enter contact number 1: ");
            String contactNumber1 = scanner.nextLine();

            System.out.print("Enter contact number 2: ");
            String contactNumber2 = scanner.nextLine();

            System.out.print("Enter address 1: ");
            String address1 = scanner.nextLine();

            System.out.print("Enter address 2: ");
            String address2 = scanner.nextLine();

            Contact contact = new Contact(name, contactNumber1, contactNumber2, address1, address2);

            myPhonebook.setPhonebook(contact);

            // Ask user if they want to add another contact
            System.out.print("Add another contact? (y/n): ");
            String input = scanner.nextLine();
            if (!input.equalsIgnoreCase("y")) {
                break;
            }
        }



        if (myPhonebook == null){
            System.out.println("Phonebook is empty!");
        }else{  // Display of Phonebook
            ArrayList<Contact> contacts = myPhonebook.getPhonebook();
            contacts.forEach(contact -> {
                System.out.println(contact.getName() + "\n" +"---------- \n" + contact.getName() +" Has the following registered numbers: \n"+ contact.getContactNumber1() +"\n"+contact.getContactNumber2() +"\n------------------------\n" + contact.getName() +" Has the following registered addresses: \n"+ "My home is in " + contact.getAddress1() +"\nMy office is in "+contact.getAddress2() + "\n========================================");

            });}



    }
}