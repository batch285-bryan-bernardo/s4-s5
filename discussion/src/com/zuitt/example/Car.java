package com.zuitt.example;

public class Car {

    // Access Modifier
        // These are used to restrict the scope of a class, constructor, variable, method or data.

        // Four Types of Access Modifiers

            // 1. Default - no keyword (Accessibility is within the package)
                    // Accessible within the package - com.zuitt.example
            // 2. Private - properties or method are only acessible within the class.
                    // Example - within Car.java
            // 3. Protected - properties and methods are only accessible by the class of the same package and the subclass present in any package.
                    // by the class of the same package and subclasses and classes in the package that is outside the package.
            // 4. Public - properties and methods can be accessed anywhere.

    // Class Creation

        // Four parts of class creation
        // 1. Properties - are characteristics of objects.

    private String name;
    private String brand;
    private int yearOfMake;

    // Make a driver component of the car
    private Driver driver;




        // 2. Constructor - used to create/instantiate an object

            // a. Empty Constructor - creates an object that doesn't have any arguments or parameters.
            // instantiate an object from the car class without declaring properties in order for us to do that, we are going to create an empty constructor:
            // Also known as the default constructor.
        public Car(){
            this.yearOfMake = 2000;
            // added Driver
            this.driver = new Driver("Badong");
        }


            // b. Parameterized Constructor - creates an object with arguments/parameters.

        public Car(String name, String brand, int yearOfMake){

        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
        this.driver = new Driver();
    }

        // 3. Getters and Setters - get and set the values of each property of the object.

            // a. Getters - retrieve the value of an instantiated object.

        public String getName(){
            return this.name;
        }
        public String getBrand(){
            return this.brand;
        }
        public int getYearOfMake(){
            return this.yearOfMake;
        }

        // 4. Setters - used to change the default value of the instantiated object

        public void setName(String name){
            this.name = name;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public void setYearOfMake(int yearOfMake) {
            if(yearOfMake <= 2023){
                this.yearOfMake = yearOfMake;
            }
        }

        public String getDriverName(){
            return this.driver.getName();
        }

        public void setDriverName(String driver){
            this.driver.setName(driver);
        }

        // 4. Methods - functions an object can perform

        public void drive(){
            System.out.println("The car is running. Vroom. Vroom.");
        }

}
