package com.zuitt.example;

public class Child extends Parent{

    public void speak(){
        // We have the original method from the parent class
        super.speak();
        System.out.println("I am the child");
    }
}
